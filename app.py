from datetime import datetime
from datetime import timedelta
from fileinput import filename
import os
import pandas as pd
import requests
import boto3
import botocore
from botocore.exceptions import ClientError
import schedule
import time
import logging

logging.basicConfig(filename ='app.log', level = logging.INFO)
      
# upload file to S3
def upload_file_to_s3(file_name, bucket, file_key, extra_args):
    
    # Function to upload a file to an S3 bucket
    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file(file_name, bucket, file_key, extra_args)
    except botocore.exceptions.ClientError as error:
        raise error
    except botocore.exceptions.ParamValidationError as error:
        raise ValueError('The parameters you provided are incorrect: {}'.format(error))
    except Exception as e:
        print("Oops!", e.__class__, "occurred.")


def workflow():
    # Yesterday date
    yesterday = datetime.now() - timedelta(1)
    filename = yesterday.strftime('%m-%d-%Y') + str('.csv')

    # download full dataset
    url = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/' + str(filename)
    df = pd.read_csv(url)

    daily_ds = df
    daily_ds.to_csv(filename)

    # 1. upload original dataset to the daily directory in S3

    s3_bucket = os.getenv("s3_bucket_name")
    file_key  = 'daily/' + str(filename)

    upload_file_to_s3(filename, s3_bucket, file_key, None)
    logging.info('Full dataset successfully uploaded to S3')

    # 2. filter out the dataset for 'Czech Republic"
    htmlfile = df.loc[df['Country_Region'] == 'Czechia']

    htmlfile.to_html("result.html")
    result_file = htmlfile.to_html()

    upload_file_to_s3('result.html', s3_bucket, 'index.html', {'ContentType':'text/html'})
    logging.info('Extracted data successfully uploaded to S3')

try:
    schedule.every(1).days.do(workflow) # run daily
except Exception as e:
    print("Oops!", e.__class__, "occurred.")

while True:
    schedule.run_pending()
    time.sleep(3600)

