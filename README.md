# python-app

A sample Python app that reads a dataset from a particular source and displays the result as HTML served via AWS S3 static website

#### Obejctive
The aim of this guide is to explain design considerations chosen for the solution.

* The fundamental aspects of the solution is High Availability(HA), Fault-tolerance and Scalability. Additionally, we should be able to perform zero-downtime automated deployment. In short, we need to ensure that the design can withstand outages and faults in the underlying infrastructure, in addition to human errors during the deployment.


#### Design Considerations:

* Based on the given requirements, it is possible to implement the solution using below workflow.
* The Python app serves as backend and deployed as a container in private ECS Fargate setup. Since, the container is stateless, Fargate has been chosen.
* User will access S3 website endpoint to view the HTML result
* Python backend container will daily read the covid19-dataset, extract the info for "Country=Czechia" and upload the extracted data as HTML to the S3 bucket which is configured as a static website

```bash

                reads dataset
Python App --------------------------> Github Covid19 DataSet
    |
    |       uploads result html
    |--------------------------------> S3 Static Website <--------- User


Frontend:
    - S3(static website)
    - stateful

Backend:
    - Python backend app
    - stateless
    - HA and scalable

```

###### Network:

* A VPC has been setup with private and public subnets spread across 2 different availability zones. Since, AZs are isolated data-centers, this will help to achieve not only, high availability but fault tolerance as well. The number of availability zones is parameterized. 
* NAT gateways has been used so ECS fargate services can access ECR to download the docker images for containers. It also enables ECS to upload files to S3.

###### ECS Fargate:

* AWS Fargate is a compute engine for Amazon ECS that allows you to run containers without having to manage servers or clusters. With AWS Fargate, we no longer have to provision, configure, and scale clusters of virtual machines to run containers. 
* An ECS cluster with a single service for Python backend app is set-up in the private subnet.
* Fargate support rolling deployment which is essentially zero-downtime deployment. At any point during the deployment, atleast 1 container will be running to serve the incoming traffic.
* Autoscaling for both these servies is enable with default settings for policies, alarms, desired count, minimum count and maximum count. All these attributes are paratermised.
* Container insights has been enabled which will provide monitoring metric for the containers of both services, web and api.

###### Logging:

* Containers logs will be streamed to CloudWatch where it be view and searched through.

###### Monitoring:

* Cloudwatch has been to provided monitoring solution.
* It has been intergrated with all the tiers to provide metrics and insights into the behavior of the respective tiers.
* Moreover, alarms have also been configued to notify of certain events.

###### CI/CD
 
* Gitlab will trigger a build upon a push to the "main" branch and upload the docker image to ECR, followed by a deployment to the configured ECS Fargate service
* AWS credentials are saves masked variables

#### Improvements/ToDOs:
* The Python code could be improved. For ex:, better error handling and making it more generic by parameterizing the "Country" field in order to dispaly any country's data on S3
* Unit test cases should be added
* security check for any vulnerability of code or docker image should be implemented
* Gitlab workflow could be improved in terms of better execution model and security

